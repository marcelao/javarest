package com.example.demo.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "user")
public class User {
	@Id
	@GeneratedValue
	private Long id;
	@Column(name = "name", nullable=false)
	private String name;
	@Column(name = "cpf", nullable=false)
	private String cpf;
	
	@OneToMany(mappedBy = "user", targetEntity = Skill.class, 
				fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	List<Skill> skills = new ArrayList<>();
	
	public User(String name, String cpf, List<Skill> skills) {
		this.name = name;
		this.cpf = cpf;
		this.skills = skills;
	}
	
	public User() {}
	
	public List<Skill> getSkills() {
		return skills;
	}

	public void setSkills(List<Skill> skills) {
		this.skills = skills;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	
}
