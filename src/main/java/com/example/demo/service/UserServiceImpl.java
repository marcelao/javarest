package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.domain.User;
import com.example.demo.repository.UserRepository;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService{

	@Autowired
	private UserRepository userRepository;
	
	public User findById(long id) {
		return userRepository.getOne(id);
	}
	
	public User findByCpf(String cpf) {
		return userRepository.findByCpf(cpf);
	}
	public User findByName(String name) {
		return userRepository.findByName(name);
	}
	
	public void saveUser(User user) {
		userRepository.save(user);
	}

	public void updateUser(User user) {
		
	}

	public void deleteUserById(long id) {}
	
	public List<User> findAllUsers() {
		return null;
	}

	public void deleteAllUsers() {}

	public boolean existingUser(User user) {
		if(findByName(user.getName()) != null) {
			return true;
		}else if(findByCpf(user.getCpf()) != null) {
			return true;
		}
		return false;
	}
	
}
