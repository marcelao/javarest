package com.example.demo.service;

import com.example.demo.domain.User;

public interface UserService {

	User findById(long id);
    
    User findByName(String name);
     
    void saveUser(User user);
     
    void updateUser(User user);
     
    void deleteUserById(long id);
    
    User findByCpf(String cpf);

	public boolean existingUser(User user);
}
