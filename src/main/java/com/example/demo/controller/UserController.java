package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.domain.User;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.UserService;


@RestController
@RequestMapping(path = "/user")
public class UserController{
	
	@Autowired
	UserRepository userRepository;
	@Autowired
	UserService userService;
	
	@GetMapping(path = "/list")
	public List<User> list() {		
		return userRepository.findAll();
	}
	
	@GetMapping
	public Optional<User> searchById(@RequestParam Long id) {
		return userRepository.findById(id);
	}
	
	@PostMapping
	public boolean saveUser(@RequestBody User user) {
		System.out.println(user.getCpf() + " :: " + user.getSkills());
		if(userService.existingUser(user)) {
			return false;
		}
		userRepository.save(user);
		return true;
	}
	
	@DeleteMapping
	public boolean deleteUser(@RequestParam Long id) {
		if(userService.findById(id) != null){
			userRepository.deleteById(id);
			return true;
		}
		return false;
	}
}
