'use strict'

var module = angular.module('myApp', [])
module.controller('UserController', function($http, $scope, $window){
		
	$scope.skills = [];	
	
	$scope.userDto = {
            cpf: null,
            name: null,
            skillDtos: []
        };
	
	$scope.userDto.skillDtos = $scope.skills.map(newSkill => {
        return {
            skillId: null,
            skillName: newSkill
        };
	});
	
	$scope.init = function(){
		$scope.list();		
	}
	
	$scope.addSkill = function() {
		$scope.skills.push({'name': $scope.newSkill, 'done':false})
		$scope.newSkill = ''
	}

	$scope.deleteSkill = function(index) {	
		$scope.skills.splice(index, 1);
	}
	
	$scope.saveUser = function(userDto){
		console.log("skills", $scope.skills);
		$http({
			method: 'POST',
			url: "http://localhost:8080/user",
			data: {
				name: userDto.name,
				cpf: userDto.cpf,
				
			}
		}).success(function(response){			
			$scope.list();
		}).error(function(http, status){
			$window.alert("n deu boa" + status);
		});
	};
	
	$scope.deleteUser = function(user){		
		$http({
			method: 'DELETE',
			url: "http://localhost:8080/user",
			params: { id: user.delId }
		}).then(function(response){
			$scope.list();
			$window.alert("Removing...");			
		});
	};
	
	$scope.list = function(){
		$http.get('/user/list', {}).then(function(response){
			//console.log(response.data);
			$scope.users = response.data;				
		});
	};
	
	$scope.searchById = function(user){	
		$http.get('/user',{					
			params: { id: user.id }
		}).then(function(response){
			console.log("encontrado: ", response.data)
			$scope.userFound = response.data
		});
	};
});
